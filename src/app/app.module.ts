import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import {routing, appRoutingProviders} from './app.routing';
import { FormsModule } from '@angular/forms';
import {HttpClientModule } from '@angular/common/http';
import { MDBBootstrapModule } from 'angular-bootstrap-md';


import { AppComponent } from './app.component';
import { MainComponent } from './main/main.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { ListadoComponent } from './listado/listado.component';
import { ChildListadoAddUserComponent } from './listado/child-listado-add-user/child-listado-add-user.component';
import { ModalComponent } from './modal/modal.component';
import { GroupPipe } from './pipes/group.pipe';


@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    NotFoundComponent,
    ListadoComponent,
    ChildListadoAddUserComponent,
    ModalComponent,
    GroupPipe
  ],
  imports: [
    BrowserModule,
    routing,
    FormsModule,
    HttpClientModule,
    MDBBootstrapModule.forRoot()
  ],
  schemas: [ NO_ERRORS_SCHEMA ],
  providers: [appRoutingProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
