import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'group'
})
export class GroupPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    const newArray = [];
    if (args !== '') {
      for (const row of value) {
        console.log(row);
        if (row.group === args) {
          newArray.push(row);
        }
      }
      return newArray;
    } else {
      return value;
    }


  }

}
