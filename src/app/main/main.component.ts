import {Component, OnInit, Output} from '@angular/core';
import {Router} from '@angular/router';
import {UserModel} from '../models/user-model';
import {DaycareService} from '../services/daycare.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
  providers: [DaycareService]
})
export class MainComponent implements OnInit {

  public user: UserModel;
  public token: string;

  @Output() isModalShown: boolean;

  constructor(private _postService: DaycareService, private _route: Router) {
    // creo el objeto user con los valores ya declarados
    this.user = new UserModel('federico', '12341234');
    this.isModalShown = false;
  }

  ngOnInit() {

  }

  onSubmit() {
    console.log('submitting');
    this._postService.doLogin(this.user)
      .subscribe(
        result => {
          this.token = result;
        },
        error => {
          console.log('Error en login:');
          this.isModalShown = true;
          setTimeout(() => {
            this.isModalShown = false;
          }, 3000);
          console.log(error);
        },
        () => {
          console.log('finalizado login OK');
          const d = new Date();
          d.setTime(d.getTime() + (5 * 60 * 1000));
          const expires = 'expires=' + d.toUTCString();
          document.cookie = 'token =' + this.token + ';' + expires + ';path=/';
          this._route.navigate(['/listado']);
        }
      );
  }

}
