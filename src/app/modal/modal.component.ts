import {Component, OnInit, Input, ViewChild} from '@angular/core';
import {ModalDirective} from 'angular-bootstrap-md/index';


@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {
  @ViewChild('autoShownModal') public autoShownModal: ModalDirective;
  @Input() isModalShown: boolean;


  constructor() {
    this.isModalShown = false;
  }

  ngOnInit() {
  }


  public hideModal(): void {
    this.autoShownModal.hide();
  }

  public onHidden(): void {
    this.isModalShown = false;
  }
}
