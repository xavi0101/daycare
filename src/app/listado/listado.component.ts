import {Component, OnInit, Output, Input, IterableDiffers, DoCheck} from '@angular/core';
import {Router} from '@angular/router';
import {UserList} from '../models/user-list';
import {DaycareService} from '../services/daycare.service';
import {GroupPipe} from '../pipes/group.pipe';

@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html',
  styleUrls: ['./listado.component.scss'],
  providers: [DaycareService, GroupPipe]
})

export class ListadoComponent implements OnInit, DoCheck {
  public token: string;
  @Input() listado: UserList [];
  @Input() quieroEditar: boolean;
  @Input() newUser: UserList;
  @Input() editUser: number;
  @Output() isModalShown: boolean;
  public filter: string;
  public cookieExists: boolean;
  public differ: any;

  constructor(private _postService: DaycareService, private _route: Router, differs: IterableDiffers) {
    this.token = '';
    this.differ = differs.find([]).create(null);
    this.listado = [];
    this.filter = '';
    this.cookieExists = false;
    this.isModalShown = false;
    this.quieroEditar = false;
    this.isModalShown = false;
    this.newUser = new UserList('Joan', 'texto desc', 'A');
    this.editUser = 999;

  }

  ngOnInit() {
    this.cookieExists = this.checkCookie();
    if (this.cookieExists) {
      this.getListado();
    } else {
      this._route.navigate(['/']);
    }
  }

// Implemento doCheck junto a iterableDiffers para detectar cambios en el array si se añaden o borran elementos.
  ngDoCheck() {
    const change = this.differ.diff(this.listado);
    if (change) {
      this.updateList();
      console.log('hago uptade auto');
    }
  }

  getListado() {
    const cookie = document.cookie;
    const testCookie = cookie.search('token=');
    if (testCookie !== -1) {
      this.token = cookie.slice(6);
      this._postService.getList(this.token)
        .subscribe(
          result => {
            const response = result;
            this.listado = response;
          },
          error => {
            console.log('Error al obtener listado:');
            this.isModalShown = true;
            setTimeout(() => {
              this.isModalShown = false;
            }, 3000);

          },
          () => {
            console.log('finalizado getList OK');
          }
        );
    } else {
      this._route.navigate(['/']);
    }
  }

  // Como el diff no detecta modificaciones de los elementos existentes, le añado a la función el parámetro opcional,
  // esto hace perder el token del parent con lo cual, vuelvo a rescatar el valor (toDo: como no perder el token).
  updateList(newList?: any) {
    if (newList) {
      this.listado = newList;
      const cookie = document.cookie;
      const testCookie = cookie.search('token=');
      this.token = cookie.slice(6);
    }
    this._postService.addUser(this.listado, this.token)
      .subscribe(
        result => {
          console.log(result);
        },
        error => {
          console.error('Error al añadir ususario:');
          alert('error al actualizar');
          this.isModalShown = true;
          setTimeout(() => {
            this.isModalShown = false;
          }, 3000);
        },
        () => {
          console.log('finalizado updateList OK');

        }
      );
  }

  checkCookie() {
    const cookie = document.cookie;
    const testCookie = cookie.search('token=');
    if (testCookie !== -1) {
      this.token = cookie.slice(6);
      return true;
    } else {
      return false;
    }
  }

  edit(i) {
    this.newUser = this.listado[i];
    this.editUser = i;
    this.quieroEditar = !this.quieroEditar;
  }


}
