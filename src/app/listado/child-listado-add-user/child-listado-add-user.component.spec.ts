import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildListadoAddUserComponent } from './child-listado-add-user.component';

describe('ChildListadoAddUserComponent', () => {
  let component: ChildListadoAddUserComponent;
  let fixture: ComponentFixture<ChildListadoAddUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChildListadoAddUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildListadoAddUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
