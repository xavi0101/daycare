import {Component, OnInit, Input, Output} from '@angular/core';
import {UserList} from '../../models/user-list';
import {ListadoComponent} from '../listado.component';


@Component({
  selector: 'app-child-listado-add-user',
  templateUrl: './child-listado-add-user.component.html',
  styleUrls: ['./child-listado-add-user.component.scss']
})
export class ChildListadoAddUserComponent extends ListadoComponent implements OnInit {

  @Input() newUser: UserList;
  @Input() userList;
  @Output() quieroEditar;
  @Input() editUser;


  ngOnInit() {

  }

  onSubmit() {
    const newData = new UserList(this.newUser.name, this.newUser.desc, this.newUser.group);
    this.userList.push(newData);
  }

  updateUSer() {
    this.quieroEditar = true;
    this.userList[this.editUser] = this.newUser;
    this.updateList(this.userList);

  }
}
