import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {UserModel} from '../models/user-model';
import 'rxjs/add/operator/map';


import {Observable} from 'rxjs/Observable';

@Injectable()
export class DaycareService {

  public apiUrl: string;

  constructor(private _http: HttpClient) {
    this.apiUrl = 'https://vj1196g0q1.execute-api.eu-west-1.amazonaws.com/dev';
  }


  doLogin(user: UserModel): Observable<any> {
    return this._http.post(this.apiUrl + '/user/login', user);
  }

  getList(token: string): Observable<any> {
    return this._http.get(this.apiUrl + '/data/load', {headers: new HttpHeaders().set('Authorization', token)})
      .map(res => res);
  }

  addUser(userToAdd: any, token: string): Observable<object> {
    return this._http.post(this.apiUrl + '/data/save',
      userToAdd,
      {headers: new HttpHeaders().set('Authorization', token)}
    );
  }

}
