import { TestBed, inject } from '@angular/core/testing';

import { DaycareService } from './daycare.service';

describe('DaycareService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DaycareService]
    });
  });

  it('should be created', inject([DaycareService], (service: DaycareService) => {
    expect(service).toBeTruthy();
  }));
});
